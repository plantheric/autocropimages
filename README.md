#AutoCropImages

A quick and dirty program to trim the white border from a set of scanned images.

##Usage

AutoCropImages can process a single image file, or a whole directory of image files.

To process a single file specify the image file name and an output file name

	AutoCropImage.exe old-image.jpg new-image.jpg

To process a whole directory specify the source directory and a directory to write the processed image to.

	AutoCropImage.exe source-image-dir processed-image-dir
	
The processed image directory must already exist.