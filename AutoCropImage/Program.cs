﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCropImage
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Count() < 2)
                return;

            string inName = args[0];
            string outName = args[1];
            if (Directory.Exists(inName) && Directory.Exists(outName))
            {
                var files = Directory.GetFiles(inName, "*.jpg");

                foreach (string inFileName in files)
                {
                    string outFileName = Path.Combine(outName, Path.GetFileName(inFileName));
                    AutoCropImage(inFileName, outFileName);
                }
            }
            else if (File.Exists(inName))
            {
                AutoCropImage(inName, outName);
            }
        }

        static void AutoCropImage(string originalFilename, string croppedFilename)
        {
            Console.WriteLine(string.Format("Cropping image: {0}", Path.GetFileName(originalFilename)));

            var originalBitmap = new Bitmap(originalFilename);

            int startLine = 0;
            int endLine = 0;
            int startColumn = 0;
            int endColumn = 0;

            for (int i = 10; i < originalBitmap.Height - 10; i++)
            {
                float line = GetLineAverage(originalBitmap, i).GetBrightness();

                if (startLine == 0 && line < 0.9)
                {
                    startLine = i;
                }
                if (line < 0.9)
                {
                    endLine = i;
                }
            }

            for (int i = 10; i < originalBitmap.Width - 10; i++)
            {
                float col = GetColumnAverage(originalBitmap, i).GetBrightness();

                if (startColumn == 0 && col < 0.9)
                {
                    startColumn = i;
                }
                if (col < 0.9)
                {
                    endColumn = i;
                }
            }

            var crop = new Rectangle(startColumn, startLine, endColumn - startColumn, endLine - startLine);
            var cropped = originalBitmap.Clone(crop, originalBitmap.PixelFormat);
            cropped.Save(croppedFilename, ImageFormat.Jpeg);
        }

        static Color GetLineAverage(Bitmap bitmap, int line)
        {
            int red = 0;
            int green = 0;
            int blue = 0;

            for (int i = 0; i < bitmap.Width; i++)
            {
                Color pixel = bitmap.GetPixel(i, line);
                red += pixel.R;
                green += pixel.G;
                blue += pixel.B;
            }

            red /= bitmap.Width;
            green /= bitmap.Width;
            blue /= bitmap.Width;

            return Color.FromArgb(red, green, blue);
        }
        static Color GetColumnAverage(Bitmap bitmap, int column)
        {
            int red = 0;
            int green = 0;
            int blue = 0;

            for (int i = 0; i < bitmap.Height; i++)
            {
                Color pixel = bitmap.GetPixel(column, i);
                red += pixel.R;
                green += pixel.G;
                blue += pixel.B;
            }

            red /= bitmap.Height;
            green /= bitmap.Height;
            blue /= bitmap.Height;

            return Color.FromArgb(red, green, blue);
        }
    }
}
